/**
 * Created by suparngupta on 4/25/15.
 */

"use strict";

require("babel/register");

var o = new (require("./orchestrator"))("test", {}, { create: true, drop: true });

o.model("../tests/Blog");
o.model("../tests/Comment", { replicas: 2 });
o.model("../tests/Friend", { replicas: { "us_east": 1, us_west: 1 }, shards: 2, primaryReplicaTag: "us_east" });

var co = require("co");
co(regeneratorRuntime.mark(function callee$0$0() {
  var x;
  return regeneratorRuntime.wrap(function callee$0$0$(context$1$0) {
    while (1) switch (context$1$0.prev = context$1$0.next) {
      case 0:
        context$1$0.next = 2;
        return o.createMigrations();

      case 2:
        x = context$1$0.sent;
        return context$1$0.delegateYield(o.runMigrations("0001"), "t0", 4);

      case 4:
        x = context$1$0.t0;

        console.log(JSON.stringify(x));

      case 6:
      case "end":
        return context$1$0.stop();
    }
  }, callee$0$0, this);
}))["catch"](function (err) {
  console.log(err);
});