/**
 * Created by suparngupta on 4/25/15.
 */

"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _ = require("lodash");
var pluralize = require("pluralize");
var Model = require("./model");
var fs = require("q-io/fs");

var docLinks = {
    table_create: "http://www.rethinkdb.com/api/javascript/table_create/"
};
var MIGRATIONS_DIR = "./migrations";
var MIGRATIONS_PREFIX = "migrations_";

var Orchestrator = (function () {

    /**
     * Creates an orchestrator to manage the Migrations.
     * @param {object} _dbName the database name
     * @param {object} _connectionProperties options to be passed to the connection (user name, password etc)
     * @param {object} [_options={drop: false}] the migration options
     * @param {Boolean} [_options.drop=false] true if un-managed tables should be deleted.
     * @param {string} [_options.migrationsDir="/project_home/migrations"] the path to migrations directory.
     */

    function Orchestrator(_dbName, _connectionProperties, _options) {
        _classCallCheck(this, Orchestrator);

        var defaultOptions = {
            drop: _options.drop ? _options.drop : false,
            create: _options.create ? _options.create : false,
            migrationsDir: (function () {
                var dir = _options.migrationsDir ? _options.migrationsDir : MIGRATIONS_DIR;
                if (!_.endsWith(dir, "/")) {
                    dir += "/";
                }
                return dir;
            })()
        };
        this.__db = _dbName;
        this.__models = {};
        this.__connection = require("rethinkdbdash")(_connectionProperties);
        this.__options = defaultOptions;
    }

    _createClass(Orchestrator, [{
        key: "model",

        /**
         * Registers a model
         * @param {string} _model the model to be registered.
         * @param {object} [_options] options passed while creating table
         * @returns {Orchestrator}
         */
        value: function model(_model, _options) {
            var m = require(_model);
            _options = _options ? _options : {};
            var options = {
                primaryKey: m.primaryKey ? m.primaryKey : Model.primaryKey,
                durability: _options.durability ? _options.durability : "hard",
                shards: _options.shards ? _options.shards : 1,
                replicas: (function () {
                    if (!_options.replicas) return 1;

                    if (typeof _options.replicas !== "object" && _.parseInt(_options.replicas) !== _options.replicas) {
                        throw new Error(m.name + ": The replicas can only be an integer or an object. See " + docLinks.table_create);
                    }
                    if (typeof _options.replicas === "number") return _.parseInt(_options.replicas);
                    if (!_.has(_options, "primaryReplicaTag")) {
                        throw new Error(m.name + ": Missing primary replica tag. See " + docLinks.table_create);
                    }
                    return _options.replicas;
                })(),
                primaryReplicaTag: (function () {
                    if (typeof _options.replicas === "object" && !_options.primaryReplicaTag) {
                        throw new Error(m.name + ": replicas object must define primaryReplica tag. See " + docLinks.table_create);
                    }
                    return _options.primaryReplicaTag;
                })(),
                renamedFrom: _options.renamedFrom
            };

            var tableName = m.table ? m.table : pluralize.plural(m.name).toLowerCase();
            this.__models[tableName] = { model: m, options: options };
            return this;
        }
    }, {
        key: "models",

        /**
         * Registers a list of models.
         * @see model
         * @param _models the list of models to be registered
         * @returns {Orchestrator}
         */
        value: function models(_models) {
            var self = this;
            _.each(_models, function (_model) {
                self.model(_model);
            });
            return this;
        }
    }, {
        key: "__createTableMigrations",

        /**
         * Creates table migrations file.
         * @returns {{create: Array}}
         * @private
         */
        value: regeneratorRuntime.mark(function __createTableMigrations() {
            var self, existingNames, definedNames, toBeAdded, toBeDropped, unchanged, results;
            return regeneratorRuntime.wrap(function __createTableMigrations$(context$2$0) {
                while (1) switch (context$2$0.prev = context$2$0.next) {
                    case 0:
                        self = this;
                        context$2$0.next = 3;
                        return self.__connection.db(self.__db).tableList().run();

                    case 3:
                        existingNames = context$2$0.sent;
                        definedNames = _.keys(self.__models);
                        toBeAdded = _.difference(definedNames, existingNames);
                        toBeDropped = _.difference(existingNames, definedNames);
                        unchanged = _.intersection(definedNames, existingNames);
                        results = {
                            create: [],
                            rename: [],
                            drop: [],
                            unchanged: []
                        };

                        //rename
                        _.forOwn(self.__models, function (_value, _key) {
                            if (_value.options.renamedFrom && _value.options.renamedFrom !== _key) {
                                results.rename.push({ renamedFrom: _value.options.renamedFrom, renamedTo: _key });
                            }
                        });

                        //create
                        if (self.__options.create) {
                            (function () {
                                _.each(toBeAdded, function (add) {
                                    results.create.push({ name: add, options: _.omit(self.__models[add].options, "renamedFrom") });
                                });
                                var renamedTo = _.pluck(results.rename, "renamedTo");
                                results.create = _.reject(results.create, function (d) {
                                    return _.contains(renamedTo, d.name);
                                });
                            })();
                        }

                        //unchanged
                        _.each(unchanged, function (un) {
                            results.unchanged.push({ name: un });
                        });

                        //drop
                        if (self.__options.drop) {
                            (function () {
                                _.each(toBeDropped, function (drop) {
                                    results.drop.push({ name: drop });
                                });
                                var renamed = _.pluck(results.rename, "renamedFrom");
                                results.drop = _.reject(results.drop, function (d) {
                                    return _.contains(renamed, d.name);
                                });
                            })();
                        }
                        results.available = !_.isEmpty(results.create) || !_.isEmpty(results.drop) || !_.isEmpty(results.rename);
                        return context$2$0.abrupt("return", results);

                    case 15:
                    case "end":
                        return context$2$0.stop();
                }
            }, __createTableMigrations, this);
        })
    }, {
        key: "__ensureMigrationDirectory",

        /**
         * Ensures that the migration directory exists
         * @private
         */
        value: regeneratorRuntime.mark(function __ensureMigrationDirectory() {
            return regeneratorRuntime.wrap(function __ensureMigrationDirectory$(context$2$0) {
                while (1) switch (context$2$0.prev = context$2$0.next) {
                    case 0:
                        context$2$0.next = 2;
                        return fs.exists(this.__options.migrationsDir);

                    case 2:
                        if (context$2$0.sent) {
                            context$2$0.next = 5;
                            break;
                        }

                        context$2$0.next = 5;
                        return fs.makeTree(this.__options.migrationsDir);

                    case 5:
                    case "end":
                        return context$2$0.stop();
                }
            }, __ensureMigrationDirectory, this);
        })
    }, {
        key: "runMigrations",
        value: regeneratorRuntime.mark(function runMigrations(migrationNo) {
            var migrationFile, contents, i, migration;
            return regeneratorRuntime.wrap(function runMigrations$(context$2$0) {
                while (1) switch (context$2$0.prev = context$2$0.next) {
                    case 0:
                        migrationFile = this.__options.migrationsDir + MIGRATIONS_PREFIX + migrationNo;
                        context$2$0.next = 3;
                        return fs.read(migrationFile);

                    case 3:
                        context$2$0.t0 = context$2$0.sent;
                        contents = JSON.parse(context$2$0.t0);
                        i = 0;

                    case 6:
                        if (!(i < contents.tables.rename.length)) {
                            context$2$0.next = 13;
                            break;
                        }

                        migration = contents.tables.rename[i];
                        context$2$0.next = 10;
                        return this.__connection.db(this.__db).table(migration.renamedFrom).config().update({ "name": migration.renamedTo }).run();

                    case 10:
                        i++;
                        context$2$0.next = 6;
                        break;

                    case 13:
                        i = 0;

                    case 14:
                        if (!(i < contents.tables.create.length)) {
                            context$2$0.next = 22;
                            break;
                        }

                        migration = contents.tables.create[i];

                        console.log(migration);
                        context$2$0.next = 19;
                        return this.__connection.db(this.__db).tableCreate(migration.name, migration.options);

                    case 19:
                        i++;
                        context$2$0.next = 14;
                        break;

                    case 22:
                        i = 0;

                    case 23:
                        if (!(i < contents.tables.drop.length)) {
                            context$2$0.next = 30;
                            break;
                        }

                        migration = contents.tables.drop[i];
                        context$2$0.next = 27;
                        return this.__connection.db(this.__db).tableDrop(migration.name);

                    case 27:
                        i++;
                        context$2$0.next = 23;
                        break;

                    case 30:
                        return context$2$0.abrupt("return", contents);

                    case 31:
                    case "end":
                        return context$2$0.stop();
                }
            }, runMigrations, this);
        })
    }, {
        key: "createMigrations",

        /**
         * Runs the migrations.
         * @returns {*}
         */
        value: regeneratorRuntime.mark(function createMigrations() {
            var result, fileList, counter, newFileName;
            return regeneratorRuntime.wrap(function createMigrations$(context$2$0) {
                while (1) switch (context$2$0.prev = context$2$0.next) {
                    case 0:
                        result = {};
                        return context$2$0.delegateYield(this.__createTableMigrations.call(this), "t0", 2);

                    case 2:
                        result.tables = context$2$0.t0;
                        context$2$0.next = 5;
                        return this.__ensureMigrationDirectory.call(this);

                    case 5:
                        context$2$0.next = 7;
                        return fs.list(this.__options.migrationsDir);

                    case 7:
                        fileList = context$2$0.sent;
                        counter = fileList.length + 1;
                        newFileName = MIGRATIONS_PREFIX + _.padLeft(counter, 4, 0);

                        while (_.contains(fileList, newFileName)) {
                            newFileName = this.__options.migrationsDir + MIGRATIONS_PREFIX + _.padLeft(++counter, 4, 0);
                        }
                        context$2$0.next = 13;
                        return fs.write(this.__options.migrationsDir + newFileName, JSON.stringify(result));

                    case 13:
                        return context$2$0.abrupt("return", result);

                    case 14:
                    case "end":
                        return context$2$0.stop();
                }
            }, createMigrations, this);
        })
    }]);

    return Orchestrator;
})();

module.exports = Orchestrator;

//run tables migrations
//rename

//create

//drop