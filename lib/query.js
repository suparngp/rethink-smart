/**
 * Created by suparngupta on 4/26/15.
 */
"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var _ = require("lodash");

var Query = (function () {
    function Query(_connection, _constructor) {
        _classCallCheck(this, Query);

        this.__connection = _connection;
        this.__constructor = _constructor;
    }

    _createClass(Query, [{
        key: "findAll",

        /**
         * Finds all the documents.
         * @returns {Query}
         */
        value: function findAll() {
            return this;
        }
    }, {
        key: "__parseResults",

        /**
         * Parses the results from a list/object into Data Model
         * @param results the results from the query.
         * @returns {Model|Array} the list or an instance of Model
         * @private
         */
        value: function __parseResults(results) {
            var _this = this;

            if (!results) {
                return;
            } else if (_.isArray(results)) {
                var _ret = (function () {
                    var models = [];
                    results.forEach(function (item) {
                        models.push(new _this.__constructor(item));
                    });
                    return {
                        v: models
                    };
                })();

                if (typeof _ret === "object") return _ret.v;
            }
            return new this.__constructor(results);
        }
    }, {
        key: "run",

        /**
         * Runs the query
         * @returns {Model|Array}
         */
        value: regeneratorRuntime.mark(function run() {
            var results;
            return regeneratorRuntime.wrap(function run$(context$2$0) {
                while (1) switch (context$2$0.prev = context$2$0.next) {
                    case 0:
                        context$2$0.next = 2;
                        return this.__connection.run();

                    case 2:
                        results = context$2$0.sent;
                        return context$2$0.abrupt("return", this.__parseResults(results));

                    case 4:
                    case "end":
                        return context$2$0.stop();
                }
            }, run, this);
        })
    }]);

    return Query;
})();

//attach wrappers to the query.
var wrappers = ["get", "getAll", "between", "filter", "pluck", "delete", "without", "merge", "append", "prepend", "difference", "setInsert", "setUnion", "setIntersection", "setDifference", "getField", "hasFields", "insertAt", "spliceAt", "deleteAt", "changeAt", "keys", "literal", "object"];

wrappers.forEach(function (wrap) {
    Query.prototype[wrap] = function () {
        this.__connection = this.__connection[wrap].apply(this.__connection, arguments);
        return this;
    };
});

module.exports = Query;