/**
 * Created by suparngupta on 4/22/15.
 */
/**
 * Any db table must extend Model
 */

"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ConnectionManager = require("./connection-manager");
var _ = require("lodash");
var pluralize = require("pluralize");
var Query = require("./query");

var Model = (function () {
    function Model(_document) {
        _classCallCheck(this, Model);

        this.__resetModel(_document);
    }

    _createClass(Model, [{
        key: "__resetModel",

        /**
         * Resets the model to the supplied document
         * @param _document the initialization document
         * @private
         */
        value: function __resetModel(_document) {
            this.__model = {
                attributes: _document ? _document : {},
                replaced: {},
                updated: {}
            };
        }
    }, {
        key: "primaryKey",

        /**
         * Returns the primary key
         * @returns {*}
         */
        get: function () {
            return this.constructor.primaryKey ? this.constructor.primaryKey : Model.defaultPrimaryKey;
        }
    }, {
        key: "versionKey",

        /**
         * Returns the version key for the document.
         * @returns {*|string|string|string}
         */
        get: function () {
            return this.constructor.versionKey || Model.defaultVersionKey;
        }
    }, {
        key: "__table",

        /**
         * Gets the table name from the constructor
         * @returns {string} the table name
         * @private
         */
        get: function () {
            return this.constructor.table ? this.constructor.table : pluralize.plural(this.constructor.name.toLowerCase());
        }
    }, {
        key: "table",

        /**
         * Gets the table name from the constructor
         * @returns {string} the table name
         */
        get: function () {
            return this.constructor.table ? this.constructor.table : pluralize.plural(this.constructor.name.toLowerCase());
        }
    }, {
        key: "db",
        get: function () {
            return this.constructor.db ? this.constructor.db : "test";
        }
    }, {
        key: "id",

        /**
         * Gets the primary key of the document.
         * @returns {*}
         */
        get: function () {
            return this.toJSON()[this.primaryKey];
        }
    }, {
        key: "model",

        /**
         * Exposes the data model as a property instead of a function. Convenience method for toJSON().
         * @returns {*}
         */
        get: function () {
            return this.__model.attributes;
        }
    }, {
        key: "__connection",

        /**
         * Returns the database connection for this model
         * @returns {connection} the db connection
         * @private
         */
        get: function () {
            return ConnectionManager.connectionFromPool().db(this.db).table(this.__table);
        }
    }, {
        key: "set",

        /**
         * Sets the property to the supplied value.
         * If the property doesn't exist, it creates a new one,
         * other wise, it updates the existing value.
         * If the property existed before but has been replaced,
         * calling set will bring back the property on the model
         * and replace will be cancelled.
         * @param _property
         * @param _value
         */
        value: function set(_property, _value) {
            this.__model.updated[_property] = _value;
            delete this.__model.replaced[_property];
        }
    }, {
        key: "unset",

        /**
         * Remove the property from the document. A copy is retained in case the model
         * is reverted.
         * @param _property
         */
        value: function unset(_property) {
            var key = this.primaryKey ? this.primaryKey : Model.defaultPrimaryKey;
            if (_property === key) {
                //don't delete the id
                throw new Error("Are you trying to remove the document? Use delete() instead.");
            }

            this.__model.replaced[_property] = true;
            delete this.__model.updated[_property];
        }
    }, {
        key: "__update",

        /**
         * Updates a document in the db
         * @param updates the updates to be applied
         * @param options update options
         * @returns {Model} the updated model
         * @private
         */
        value: regeneratorRuntime.mark(function __update(updates, options) {
            var result;
            return regeneratorRuntime.wrap(function __update$(context$2$0) {
                while (1) switch (context$2$0.prev = context$2$0.next) {
                    case 0:
                        context$2$0.next = 2;
                        return this.__connection.get(this.__model.attributes[this.primaryKey]).update(updates, options).run();

                    case 2:
                        result = context$2$0.sent;

                        if (result.replaced > 0) {
                            this.__resetModel(_.last(result.changes).new_val);
                        }
                        return context$2$0.abrupt("return", this);

                    case 5:
                    case "end":
                        return context$2$0.stop();
                }
            }, __update, this);
        })
    }, {
        key: "__replace",

        /**
         * Performs replace operation on the documents
         * @param replacements the replacements to be performed
         * @param options the replace options
         * @returns {Model} the updated model
         * @private
         */
        value: regeneratorRuntime.mark(function __replace(replacements, options) {
            var result;
            return regeneratorRuntime.wrap(function __replace$(context$2$0) {
                while (1) switch (context$2$0.prev = context$2$0.next) {
                    case 0:
                        context$2$0.next = 2;
                        return this.__connection.get(this.__model.attributes[this.primaryKey]).replace(function (document) {
                            return document.without(replacements);
                        }, options).run();

                    case 2:
                        result = context$2$0.sent;

                        if (result.replaced > 0) {
                            this.__resetModel(_.last(result.changes).new_val);
                        }
                        return context$2$0.abrupt("return", this);

                    case 5:
                    case "end":
                        return context$2$0.stop();
                }
            }, __replace, this);
        })
    }, {
        key: "__insert",

        /**
         * Inserts a new document in the db
         * @param insertion the document model
         * @param options the insertion options
         * @returns {Model} the updated document
         * @private
         */
        value: regeneratorRuntime.mark(function __insert(insertion, options) {
            var result;
            return regeneratorRuntime.wrap(function __insert$(context$2$0) {
                while (1) switch (context$2$0.prev = context$2$0.next) {
                    case 0:
                        context$2$0.next = 2;
                        return this.__connection.insert(insertion, options).run();

                    case 2:
                        result = context$2$0.sent;

                        if ((result.replaced > 0 || result.inserted > 0) && result.changes && result.changes.length) {
                            this.__resetModel(_.last(result.changes).new_val);
                        }
                        return context$2$0.abrupt("return", this);

                    case 5:
                    case "end":
                        return context$2$0.stop();
                }
            }, __insert, this);
        })
    }, {
        key: "__runQuery",

        /**
         * Runs the query pipeline. Pipeline can be a set of update and replace operations.
         * @param _query the query pipeline
         * @returns {Model} the updated model
         * @private
         */
        value: regeneratorRuntime.mark(function __runQuery(_query) {
            var self, result, fns, i, fn;
            return regeneratorRuntime.wrap(function __runQuery$(context$2$0) {
                while (1) switch (context$2$0.prev = context$2$0.next) {
                    case 0:
                        self = this;
                        result = undefined;
                        fns = _.keys(_query);
                        i = 0;

                    case 4:
                        if (!(i < fns.length)) {
                            context$2$0.next = 12;
                            break;
                        }

                        fn = fns[i];
                        context$2$0.next = 8;
                        return self[fn].apply(self, _query[fn]);

                    case 8:
                        result = context$2$0.sent;

                    case 9:
                        i++;
                        context$2$0.next = 4;
                        break;

                    case 12:
                        return context$2$0.abrupt("return", this);

                    case 13:
                    case "end":
                        return context$2$0.stop();
                }
            }, __runQuery, this);
        })
    }, {
        key: "toJSON",

        /**
         * Get the JSON version of the model
         * @returns {object}
         */
        value: function toJSON() {
            return this.__model.attributes;
        }
    }, {
        key: "__objectify",

        /**
         * creates an object from the dotted notation.
         * @param obj the object containing dotted keys
         * @returns {object}
         * @private
         */
        value: function __objectify(obj) {
            var objectified = {};
            _.each(_.forOwn(obj), function (_value, _key) {
                _.set(objectified, _key, _value);
            });
            return objectified;
        }
    }, {
        key: "save",

        /**
         * Saves the model in the db.
         * @returns {Model} the updated model
         */
        value: regeneratorRuntime.mark(function save() {
            var options = arguments[0] === undefined ? { returnChanges: true } : arguments[0];
            var self, query, update, replace;
            return regeneratorRuntime.wrap(function save$(context$2$0) {
                while (1) switch (context$2$0.prev = context$2$0.next) {
                    case 0:
                        self = this;
                        query = {};

                        if (!_.has(self.__model.attributes, self.versionKey)) {
                            context$2$0.next = 8;
                            break;
                        }

                        //console.log("I am in update");
                        ; //increment the version before updating.
                        if (_.keys(self.__model.updated).length) {
                            update = self.__objectify(self.__model.updated);

                            update[self.versionKey] = self.__model.attributes[self.versionKey] + 1;
                            query.__update = [update, options];
                        }

                        if (_.keys(self.__model.replaced).length) {
                            replace = self.__objectify(self.__model.replaced);

                            replace[self.versionKey] = self.__model.attributes[self.versionKey] + 1;
                            query.__replace = [replace, options];
                        }

                        //console.log(query);
                        context$2$0.next = 16;
                        break;

                    case 8:
                        _.assign(self.__model.attributes, self.__model.updated);
                        _.each(_.forOwn(self.__model.replaced), function (_value, _property) {
                            _.set(self.__model.attributes, _property, undefined);
                        });
                        self.__model.attributes[self.versionKey] = 0;

                        if (self.__model.attributes[self.primaryKey]) {
                            context$2$0.next = 15;
                            break;
                        }

                        context$2$0.next = 14;
                        return self.constructor.uuid();

                    case 14:
                        self.__model.attributes[self.primaryKey] = context$2$0.sent;

                    case 15:
                        query.__insert = [self.__model.attributes, options];

                    case 16:
                        return context$2$0.delegateYield(this.__runQuery.call(this, query), "t0", 17);

                    case 17:
                        return context$2$0.abrupt("return", context$2$0.t0);

                    case 18:
                    case "end":
                        return context$2$0.stop();
                }
            }, save, this);
        })
    }, {
        key: "delete",

        /**
         * Deletes a document
         * @returns {*|{t, r, b}}
         */
        value: regeneratorRuntime.mark(function _delete() {
            return regeneratorRuntime.wrap(function _delete$(context$2$0) {
                while (1) switch (context$2$0.prev = context$2$0.next) {
                    case 0:
                        if (this.__model.attributes[this.primaryKey]) {
                            context$2$0.next = 2;
                            break;
                        }

                        throw new Error("Bing! You cannot delete something that doesn't exist. Save it first.");

                    case 2:
                        return context$2$0.abrupt("return", this.__connection.get(this.__model.attributes[this.primaryKey])["delete"]().run());

                    case 3:
                    case "end":
                        return context$2$0.stop();
                }
            }, _delete, this);
        })
    }, {
        key: "sync",

        /**
         * Syncs a given document.
         * @returns {object.synced} 1 if sync was successful
         */
        value: regeneratorRuntime.mark(function sync() {
            return regeneratorRuntime.wrap(function sync$(context$2$0) {
                while (1) switch (context$2$0.prev = context$2$0.next) {
                    case 0:
                        if (this.__model.attributes[this.primaryKey]) {
                            context$2$0.next = 2;
                            break;
                        }

                        throw new Error("Bing! You cannot sync something that doesn't exist. Save it first.");

                    case 2:
                        context$2$0.next = 4;
                        return this.__connection.sync().run();

                    case 4:
                        return context$2$0.abrupt("return", context$2$0.sent);

                    case 5:
                    case "end":
                        return context$2$0.stop();
                }
            }, sync, this);
        })
    }], [{
        key: "db",
        get: function () {
            return "test";
        }
    }, {
        key: "defaultPrimaryKey",

        /**
         * Default primary key
         * @returns {string}
         */
        get: function () {
            return "id";
        }
    }, {
        key: "defaultVersionKey",

        /**
         * Returns the default version key
         * @returns {string}
         */
        get: function () {
            return "__v";
        }
    }, {
        key: "__connection",

        /**
         * Static wrapper to get db connection for this table
         * @returns {*|{node, tbody, thead, appendChild, addRow}|{style}|{borderCollapse, width}}
         * @private
         */
        get: function () {
            var tableName = this.table ? this.table : pluralize.plural(this.name.toLowerCase());
            return ConnectionManager.connectionFromPool().db(this.db).table(tableName);
        }
    }, {
        key: "query",

        /**
         * returns a new query
         * @returns {Query|exports|module.exports}
         */
        get: function () {
            return new Query(this.__connection, this);
        }
    }, {
        key: "row",

        /**
         * Returns the row.
         * @returns {*|util.row|Function}
         */
        get: function () {
            return ConnectionManager.connectionFromPool().row;
        }
    }]);

    return Model;
})();

//attach wrappers to the query.
var wrappers = ["uuid"];

wrappers.forEach(function (wrap) {
    Model[wrap] = regeneratorRuntime.mark(function callee$1$0() {
        var conn;
        return regeneratorRuntime.wrap(function callee$1$0$(context$2$0) {
            while (1) switch (context$2$0.prev = context$2$0.next) {
                case 0:
                    conn = ConnectionManager.connectionFromPool();
                    return context$2$0.abrupt("return", conn[wrap]().run());

                case 2:
                case "end":
                    return context$2$0.stop();
            }
        }, callee$1$0, this);
    });
});

module.exports = Model;