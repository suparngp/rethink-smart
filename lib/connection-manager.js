/**
 * Created by suparngupta on 4/22/15.
 */
"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var r = require("rethinkdbdash");

var ConnectionManager = (function () {
    function ConnectionManager() {
        _classCallCheck(this, ConnectionManager);
    }

    _createClass(ConnectionManager, null, [{
        key: "init",
        value: function init(_options) {
            ConnectionManager.__connection = r(_options);
        }
    }, {
        key: "connectionFromPool",
        value: function connectionFromPool() {
            if (!ConnectionManager.__connection) {
                throw new Error("Call init(options) to setup connection");
            }
            return ConnectionManager.__connection;
        }
    }]);

    return ConnectionManager;
})();

module.exports = ConnectionManager;