/**
 * Created by suparngupta on 4/25/15.
 */
//require("babel/register");
var index = function (_options) {
    var ConnectionManager = require("./lib/connection-manager")
    ConnectionManager.init(_options);
    index.Model = require("./lib/model");
    index.ConnectionManager = ConnectionManager;
};
module.exports = index;