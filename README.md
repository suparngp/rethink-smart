Rethink-Smart
=====

A Rethink-DB ORM written in pure ES6 which uses generators and yield blocks instead of promises and callbacks

- Provides connection pool for scalability
- Super simple model creation, no need to register
- Extendable Models, just like Hibernate or any other OO ORM framework

You can create a simple model just by using:

    class Person {
	    get name(){}
    }


Check out the unit tests for more examples.