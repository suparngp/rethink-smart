/**
 * Created by suparngupta on 4/22/15.
 */
var r = require("rethinkdbdash");
class ConnectionManager{

    static init(_options){
        ConnectionManager.__connection = r(_options);
    }

    static connectionFromPool(){
        if(!ConnectionManager.__connection){
            throw new Error("Call init(options) to setup connection");
        }
        return ConnectionManager.__connection;
    }

}

module.exports = ConnectionManager;