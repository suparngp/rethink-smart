/**
 * Created by suparngupta on 4/22/15.
 */
/**
 * Any db table must extend Model
 */

var ConnectionManager = require("./connection-manager");
var _ = require("lodash");
var pluralize = require("pluralize");
var Query = require("./query");
class Model {
    constructor(_document) {
        this.__resetModel(_document);
    }

    /**
     * Resets the model to the supplied document
     * @param _document the initialization document
     * @private
     */
    __resetModel(_document) {
        this.__model = {
            attributes: _document ? _document : {},
            replaced: {},
            updated: {}
        };
    }

    static get db(){
        return "test";
    }
    /**
     * Default primary key
     * @returns {string}
     */
    static get defaultPrimaryKey() {
        return "id";
    }

    /**
     * Returns the primary key
     * @returns {*}
     */
    get primaryKey(){
        return this.constructor.primaryKey ? this.constructor.primaryKey : Model.defaultPrimaryKey;
    }

    /**
     * Returns the default version key
     * @returns {string}
     */
    static get defaultVersionKey(){
        return "__v";
    }

    /**
     * Returns the version key for the document.
     * @returns {*|string|string|string}
     */
    get versionKey(){
        return this.constructor.versionKey || Model.defaultVersionKey;
    }

    /**
     * Gets the table name from the constructor
     * @returns {string} the table name
     * @private
     */
    get __table() {
        return this.constructor.table ? this.constructor.table : pluralize.plural(this.constructor.name.toLowerCase());
    }

    /**
     * Gets the table name from the constructor
     * @returns {string} the table name
     */
    get table() {
        return this.constructor.table ? this.constructor.table : pluralize.plural(this.constructor.name.toLowerCase());
    }

    get db() {
        return this.constructor.db ? this.constructor.db : "test";
    }

    /**
     * Gets the primary key of the document.
     * @returns {*}
     */
    get id(){
        return this.toJSON()[this.primaryKey];
    }

    /**
     * Exposes the data model as a property instead of a function. Convenience method for toJSON().
     * @returns {*}
     */
    get model(){
        return this.__model.attributes;
    }

    /**
     * Static wrapper to get db connection for this table
     * @returns {*|{node, tbody, thead, appendChild, addRow}|{style}|{borderCollapse, width}}
     * @private
     */
    static get __connection() {
        let tableName = this.table ? this.table : pluralize.plural(this.name.toLowerCase());
        return ConnectionManager.connectionFromPool().db(this.db).table(tableName);
    }

    /**
     * Returns the database connection for this model
     * @returns {connection} the db connection
     * @private
     */
    get __connection() {
        return ConnectionManager.connectionFromPool().db(this.db).table(this.__table);
    }

    /**
     * Sets the property to the supplied value.
     * If the property doesn't exist, it creates a new one,
     * other wise, it updates the existing value.
     * If the property existed before but has been replaced,
     * calling set will bring back the property on the model
     * and replace will be cancelled.
     * @param _property
     * @param _value
     */
    set(_property, _value) {
        this.__model.updated[_property] = _value;
        delete this.__model.replaced[_property];
    }


    /**
     * Remove the property from the document. A copy is retained in case the model
     * is reverted.
     * @param _property
     */
    unset(_property) {
        let key = this.primaryKey ? this.primaryKey : Model.defaultPrimaryKey;
        if (_property === key) { //don't delete the id
            throw new Error("Are you trying to remove the document? Use delete() instead.");
        }

        this.__model.replaced[_property] = true;
        delete this.__model.updated[_property];
    }

    /**
     * Updates a document in the db
     * @param updates the updates to be applied
     * @param options update options
     * @returns {Model} the updated model
     * @private
     */
    * __update(updates, options) {
        let result = yield this.__connection.get(this.__model.attributes[this.primaryKey]).update(updates, options).run();
        if (result.replaced > 0) {
            this.__resetModel(_.last(result.changes).new_val);
        }
        return this;
    }

    /**
     * Performs replace operation on the documents
     * @param replacements the replacements to be performed
     * @param options the replace options
     * @returns {Model} the updated model
     * @private
     */
    * __replace(replacements, options) {
        let result = yield this.__connection.get(this.__model.attributes[this.primaryKey]).replace(function (document) {
            return document.without(replacements);
        }, options).run();
        if (result.replaced > 0) {
            this.__resetModel(_.last(result.changes).new_val);
        }
        return this;
    }

    /**
     * Inserts a new document in the db
     * @param insertion the document model
     * @param options the insertion options
     * @returns {Model} the updated document
     * @private
     */
    * __insert(insertion, options) {
        let result = yield this.__connection.insert(insertion, options).run();
        if((result.replaced > 0 || result.inserted > 0) && result.changes && result.changes.length){
            this.__resetModel(_.last(result.changes).new_val);
        }
        return this;
    }


    /**
     * Runs the query pipeline. Pipeline can be a set of update and replace operations.
     * @param _query the query pipeline
     * @returns {Model} the updated model
     * @private
     */
    * __runQuery(_query) {
        let self = this;
        let result;
        let fns = _.keys(_query);
        for (let i = 0; i < fns.length; i++) {
            let fn = fns[i];
            result = yield self[fn].apply(self, _query[fn]);
        }
        return this;
    }

    /**
     * Get the JSON version of the model
     * @returns {object}
     */
    toJSON() {
        return this.__model.attributes;
    }

    /**
     * creates an object from the dotted notation.
     * @param obj the object containing dotted keys
     * @returns {object}
     * @private
     */
    __objectify(obj) {
        let objectified = {};
        _.each(_.forOwn(obj), function (_value, _key) {
            _.set(objectified, _key, _value);
        });
        return objectified;
    }

    /**
     * Saves the model in the db.
     * @returns {Model} the updated model
     */
    * save(options = {returnChanges: true}) {
        var self = this;
        var query = {};


        if (_.has(self.__model.attributes, self.versionKey)) {
            //console.log("I am in update");
            ; //increment the version before updating.
            if (_.keys(self.__model.updated).length) {
                let update = self.__objectify(self.__model.updated);
                update[self.versionKey] = self.__model.attributes[self.versionKey] + 1;
                query.__update = [update, options];
            }

            if (_.keys(self.__model.replaced).length) {
                let replace = self.__objectify(self.__model.replaced);
                replace[self.versionKey] = self.__model.attributes[self.versionKey] + 1;
                query.__replace = [replace, options];
            }

            //console.log(query);
        }
        else {
            _.assign(self.__model.attributes, self.__model.updated);
            _.each(_.forOwn(self.__model.replaced), function (_value, _property) {
                _.set(self.__model.attributes, _property, undefined);
            });
            self.__model.attributes[self.versionKey] = 0;
            if (!self.__model.attributes[self.primaryKey]){
                self.__model.attributes[self.primaryKey] = yield self.constructor.uuid();
            }
            query.__insert = [self.__model.attributes, options];
        }
        return yield* this.__runQuery.call(this, query);
    }

    /**
     * Deletes a document
     * @returns {*|{t, r, b}}
     */
    * delete() {

        if (!this.__model.attributes[this.primaryKey]) {
            throw new Error("Bing! You cannot delete something that doesn't exist. Save it first.");
        }
        return this.__connection.get(this.__model.attributes[this.primaryKey]).delete().run();
    }

    /**
     * Syncs a given document.
     * @returns {object.synced} 1 if sync was successful
     */
    * sync() {
        if (!this.__model.attributes[this.primaryKey]) {
            throw new Error("Bing! You cannot sync something that doesn't exist. Save it first.");
        }
        return yield this.__connection.sync().run();
    }

    /**
     * returns a new query
     * @returns {Query|exports|module.exports}
     */
    static get query(){
        return new Query(this.__connection, this);
    }


    /**
     * Returns the row.
     * @returns {*|util.row|Function}
     */
    static get row(){
        return ConnectionManager.connectionFromPool().row;
    }
}

//attach wrappers to the query.
const wrappers = ["uuid"];

wrappers.forEach(wrap => {
    Model[wrap] = function* () {
        var conn = ConnectionManager.connectionFromPool();
        return conn[wrap]().run();
    };
});

module.exports = Model;