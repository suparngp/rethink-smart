/**
 * Created by suparngupta on 4/26/15.
 */
var _ = require("lodash");
class Query {
    constructor(_connection, _constructor) {
        this.__connection = _connection;
        this.__constructor = _constructor;
    }


    /**
     * Finds all the documents.
     * @returns {Query}
     */
    findAll() {
        return this;
    }

    /**
     * Parses the results from a list/object into Data Model
     * @param results the results from the query.
     * @returns {Model|Array} the list or an instance of Model
     * @private
     */
    __parseResults(results) {
        if (!results){
            return;
        }
        else if (_.isArray(results)) {
            let models = [];
            results.forEach(item => {
                models.push(new this.__constructor(item));
            });
            return models;
        }
        return new this.__constructor(results);
    }

    /**
     * Runs the query
     * @returns {Model|Array}
     */
    * run() {
        let results = yield this.__connection.run();
        return this.__parseResults(results);
    }
}

//attach wrappers to the query.
const wrappers = ["get", "getAll", "between", "filter", "pluck", "delete", "without", "merge", "append", "prepend", "difference", "setInsert",
    "setUnion", "setIntersection", "setDifference", "getField", "hasFields", "insertAt", "spliceAt", "deleteAt", "changeAt", "keys", "literal", "object"];

wrappers.forEach(wrap => {
    Query.prototype[wrap] = function () {
        this.__connection = this.__connection[wrap].apply(this.__connection, arguments);
        return this;
    };
});

module.exports = Query;