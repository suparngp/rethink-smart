/**
 * Created by suparngupta on 4/25/15.
 */

var _ = require("lodash");
var pluralize = require("pluralize");
var Model = require("./model");
var fs = require("q-io/fs");

var docLinks = {
    table_create: "http://www.rethinkdb.com/api/javascript/table_create/"
};
const MIGRATIONS_DIR = "./migrations";
const MIGRATIONS_PREFIX = "migrations_";
class Orchestrator {

    /**
     * Creates an orchestrator to manage the Migrations.
     * @param {object} _dbName the database name
     * @param {object} _connectionProperties options to be passed to the connection (user name, password etc)
     * @param {object} [_options={drop: false}] the migration options
     * @param {Boolean} [_options.drop=false] true if un-managed tables should be deleted.
     * @param {string} [_options.migrationsDir="/project_home/migrations"] the path to migrations directory.
     */
    constructor(_dbName, _connectionProperties, _options) {
        let defaultOptions = {
            drop: _options.drop ? _options.drop: false,
            create: _options.create ? _options.create: false,
            migrationsDir: (function () {
                let dir = _options.migrationsDir ? _options.migrationsDir : MIGRATIONS_DIR
                if(!_.endsWith(dir, "/")){
                    dir += "/";
                }
                return dir;
            })()
        };
        this.__db = _dbName;
        this.__models = {};
        this.__connection = require("rethinkdbdash")(_connectionProperties);
        this.__options = defaultOptions;

    }

    /**
     * Registers a model
     * @param {string} _model the model to be registered.
     * @param {object} [_options] options passed while creating table
     * @returns {Orchestrator}
     */
    model(_model, _options) {
        let m = require(_model);
        _options = _options ? _options: {};
        let options = {
            primaryKey: m.primaryKey ? m.primaryKey : Model.primaryKey,
            durability: _options.durability ? _options.durability: "hard",
            shards: _options.shards ? _options.shards : 1,
            replicas: (function () {
                if(!_options.replicas) return 1;

                if(typeof _options.replicas !== "object" && _.parseInt(_options.replicas) !== _options.replicas){
                    throw new Error(m.name + ": The replicas can only be an integer or an object. See " + docLinks.table_create);
                }
                if(typeof _options.replicas === "number") return _.parseInt(_options.replicas);
                if(!_.has(_options, "primaryReplicaTag")){
                    throw new Error(m.name + ": Missing primary replica tag. See " + docLinks.table_create);
                }
                return _options.replicas;
            })(),
            primaryReplicaTag: (function () {
                if(typeof _options.replicas === "object" && !_options.primaryReplicaTag){
                    throw new Error(m.name + ": replicas object must define primaryReplica tag. See " + docLinks.table_create);
                }
                return _options.primaryReplicaTag;
            })(),
            renamedFrom: _options.renamedFrom
        };

        let tableName = m.table ? m.table : pluralize.plural(m.name).toLowerCase();
        this.__models[tableName] = {model: m, options: options};
        return this;
    }

    /**
     * Registers a list of models.
     * @see model
     * @param _models the list of models to be registered
     * @returns {Orchestrator}
     */
    models(_models) {
        let self = this;
        _.each(_models, function (_model) {
            self.model(_model);
        });
        return this;
    }

    /**
     * Creates table migrations file.
     * @returns {{create: Array}}
     * @private
     */
    * __createTableMigrations(){
        let self = this;
        let existingNames = yield self.__connection.db(self.__db).tableList().run();
        let definedNames = _.keys(self.__models);
        let toBeAdded = _.difference(definedNames, existingNames);
        let toBeDropped = _.difference(existingNames, definedNames);
        let unchanged = _.intersection(definedNames, existingNames);
        let results = {
            create: [],
            rename: [],
            drop: [],
            unchanged: []
        };

        //rename
        _.forOwn(self.__models, function (_value, _key) {
            if(_value.options.renamedFrom && _value.options.renamedFrom !== _key){
                results.rename.push({renamedFrom: _value.options.renamedFrom, renamedTo: _key});
            }
        });

        //create
        if(self.__options.create){
            _.each(toBeAdded, function (add) {
                results.create.push({name: add, options: _.omit(self.__models[add].options, "renamedFrom")});
            });
            let renamedTo = _.pluck(results.rename, "renamedTo");
            results.create = _.reject(results.create, function (d) {
                return _.contains(renamedTo, d.name);
            });
        }

        //unchanged
        _.each(unchanged, function (un) {
            results.unchanged.push({name: un});
        });

        //drop
        if(self.__options.drop){
            _.each(toBeDropped, function (drop) {
                results.drop.push({name: drop});
            });
            let renamed = _.pluck(results.rename, "renamedFrom");
            results.drop = _.reject(results.drop, function (d) {
                return _.contains(renamed, d.name);
            });
        }
        results.available = !_.isEmpty(results.create) || !_.isEmpty(results.drop) || !_.isEmpty(results.rename);
        return results;
    }

    /**
     * Ensures that the migration directory exists
     * @private
     */
    * __ensureMigrationDirectory(){
        if(!(yield fs.exists(this.__options.migrationsDir))){
            yield fs.makeTree(this.__options.migrationsDir);
        }
    }


    * runMigrations(migrationNo){
        let migrationFile = this.__options.migrationsDir + MIGRATIONS_PREFIX + migrationNo;
        let contents = JSON.parse(yield fs.read(migrationFile));
        //run tables migrations
        //rename
        for(let i = 0; i < contents.tables.rename.length; i++){
            let migration = contents.tables.rename[i];

            yield this.__connection.db(this.__db).table(migration.renamedFrom).config().update({"name": migration.renamedTo}).run();
        }

        //create
        for(let i = 0; i < contents.tables.create.length; i++){
            let migration = contents.tables.create[i];
            console.log(migration);
            yield this.__connection.db(this.__db).tableCreate(migration.name, migration.options);
        }

        //drop
        for(let i = 0; i < contents.tables.drop.length; i++){
            let migration = contents.tables.drop[i];
            yield this.__connection.db(this.__db).tableDrop(migration.name);
        }

        return contents;
    }

    /**
     * Runs the migrations.
     * @returns {*}
     */
    * createMigrations(){
        let result = {};
        result.tables = yield* this.__createTableMigrations.call(this);
        yield this.__ensureMigrationDirectory.call(this);

        let fileList = yield fs.list(this.__options.migrationsDir);
        let counter = fileList.length + 1;
        let newFileName = MIGRATIONS_PREFIX + _.padLeft(counter, 4, 0);
        while(_.contains(fileList, newFileName)){
            newFileName = this.__options.migrationsDir + MIGRATIONS_PREFIX + _.padLeft(++counter, 4, 0);
        }
        yield fs.write(this.__options.migrationsDir + newFileName, JSON.stringify(result));
        return result;
    }
}

module.exports = Orchestrator;