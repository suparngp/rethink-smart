/**
 * Created by suparngupta on 4/22/15.
 */

var Model = require("../../index").Model;

class Blog extends Model{
    static get table(){
        return "blogs";
    }

    static get primaryKey(){
        return "blog_id";
    }
}

module.exports = Blog;