/**
 * Created by suparngupta on 4/25/15.
 */
var Model = require("../../src/model");

class Friend extends Model{
    static get table(){
        return "girlfriends";
    }
}

module.exports = Friend;