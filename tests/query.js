/**
 * Created by suparngupta on 4/26/15.
 */
require("babel/register");
var _ = require("lodash");
describe("Query ", function () {
    var co = require("co");
    var should = require("chai").should();
    var Blog, smart;
    var blog;
    var faker = require("faker");
    before(function (done) {
        smart = require("../index")({});
        Blog = require("./models/Blog");
        blog = new Blog({
            title: faker.lorem.sentence(),
            content: faker.lorem.paragraph(),
            likes: new Date().getTime()
        });
        co(function* () {
            blog = yield blog.save();
            done();
        }).catch(error);
    });


    describe("get should", function () {
        it("exist", function (done) {
            Blog.query.get.should.be.ok;
            done();
        });

        it("return correct document", function (done) {
            co(function *() {
                var res = yield Blog.query.get(blog.id).run();
                res.should.be.an.instanceof(Blog);
                res.should.be.ok;
                res.id.should.be.ok;
                res.id.should.be.eql(blog.id);
                done();
            }).catch(error);
        });
    });

    describe("filter should", function () {
        it("exist", function () {
            Blog.query.filter.should.be.ok;
            Blog.query.filter.should.be.a("function");
        });

        it("return correct document", function (done) {
            co(function *() {
                var results = yield Blog.query.filter({likes: blog.model.likes}).run()
                results.should.be.an.instanceof(Array);
                results.length.should.be.eql(1);
                results[0].should.be.an.instanceof(Blog);
                results[0].id.should.be.eql(blog.id);
                done();
            }).catch(error);

        });
    });

    describe("between should", function () {
        it("exist", function () {
            Blog.query.between.should.be.ok;
            Blog.query.between.should.be.a("function");
        });

        it("return correct document", function (done) {
            co(function *() {
                var results = yield Blog.query.between(blog.model.likes - 4, blog.model.likes + 4, {index: "likes"}).run()
                results.should.be.an.instanceof(Array);
                results.length.should.be.eql(1);
                results[0].should.be.an.instanceof(Blog);
                results[0].id.should.be.eql(blog.id);
                done();
            }).catch(error);
        });
    });

    after(function (done) {
        co(function *() {
            yield Blog.query.delete().run();
            require("../index").ConnectionManager.connectionFromPool().getPoolMaster().drain();
            done();
        }).catch(function (err) {
            console.log(err);
        });
    });

    function error(err) {
        console.log(err.toString());
    }

});