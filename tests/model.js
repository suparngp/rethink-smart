/**
 * Created by suparngupta on 4/25/15.
 */
require("babel/register");
var co = require("co");
var should = require("chai").should();
require("../index")({});
describe("Blog", function () {

    describe("model should", function () {
        it("be initialized", function () {
            var Blog = require("./models/Blog");
            Blog.should.be.ok;
            Blog.table.should.be.eql("blogs");
            Blog.primaryKey.should.be.eql("blog_id");
        });
    });

    describe("DB operation", function () {
        var Blog;
        var sample = {
            title: "John Doe",
            content: "Hello! How are you MotherFucker"
        };
        before(function () {
            Blog = require("./models/Blog");
        });

        describe("Write", function () {
            it("Save work with initialized model", function (done) {
                co(function* () {
                    var blog = new Blog(sample);
                    yield blog.save();
                    blog.id.should.be.ok;
                    blog.model.title.should.be.eql(sample.title);
                    blog.model.content.should.be.eql(sample.content);
                    done();
                }).catch(err_catch);
            });

            it("Save should save after updating the model", function (done) {
                co(function *() {
                    var blog = new Blog(sample);
                    yield blog.save();
                    console.log(blog.toJSON());
                    blog.set("source.url", "http://reddit.com");
                    yield blog.save();
                    blog.model.source.should.be.ok;
                    blog.model.source.url.should.be.eql("http://reddit.com");
                    done();
                }).catch(err_catch);
            });

            it("Save should save after replace the model", function (done) {
                co(function *() {
                    var blog = new Blog(sample);
                    blog.set("source.url", "http://reddit.com");
                    yield blog.save();
                    blog.unset("source");
                    yield blog.save();
                    (!blog.model.source).should.be.ok;
                    done();
                }).catch(err_catch);
            });

            it("sync should sync successfully", function (done) {
                co(function *() {
                    var blog = new Blog(sample);
                    yield blog.save();
                    var sync = yield blog.sync();
                    sync.synced.should.be.eql(1);
                    done();
                }).catch(err_catch);
            });
            after(function (done) {
                co(function *() {
                    yield Blog.query.delete().run();
                    console.log("DB cleared");
                    require("../index").ConnectionManager.connectionFromPool().getPoolMaster().drain();
                    done();
                });
            })
        });
    });

    function err_catch(error) {
        console.log(error);
    }
});